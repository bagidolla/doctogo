$(function() {

    $('.owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        responsiveClass:true,
        responsive:{
            0:{
                items:0,
                nav:false
            },
            768:{
                items:3,
                nav:true,
                dots: false,
                navText: ["",""]
            },
            1200:{
                items:4,
                nav:true,
                dots: false,
                loop:false,
                navText: ["",""],
                margin: 20
            }
        }
    });

    $('.selectpicker').selectpicker();

    $('#menuCollapse').on('show.bs.collapse', function () {
        $('html, body').css({
            'height': '100%',
            'overflow': 'hidden'
        });
    });
    $('#menuCollapse').on('hidden.bs.collapse', function () {
        $('html, body').css({
            'height': 'initial',
            'overflow': 'initial'
        });
    });

    $('.add_feedback_mobile').click(function() {
        $('.feedback_collapse_mobile').show();
        $('html, body').css({
            'height': '100%',
            'overflow': 'hidden'
        });
    });

    $('.close__mobile').click(function() {
        $('.feedback_collapse_mobile').hide();
        $('html, body').css({
            'height': 'initial',
            'overflow': 'initial'
        });
    });

    $('.enroll_link').click(function() {
        $('.enroll_mobile').show();
        $('html, body').css({
            'height': '100%',
            'overflow': 'hidden'
        });
    });

    $('.enroll_mobile_close').click(function() {
        $('.enroll_mobile').hide();
        $('html, body').css({
            'height': 'initial',
            'overflow': 'initial'
        });
    });

    $('.dropdown-toggle_mobile').click(function() {
        $('.search_mobile').show();
        $('html, body').css({
            'height': '100%',
            'overflow': 'hidden'
        });
    });

    $('.search_mobile__close').click(function() {
        $('.search_mobile').hide();
        $('html, body').css({
            'height': 'initial',
            'overflow': 'initial'
        });
    });

    $('.filter_toggle').click(function() {
        $('.filters').toggleClass('hidden-xs');
    });

    //Phones Masks
    $("[name='phone']").mask("+7 (999) 999-99-99");

    //Datepicker
    $('.date-control .date_picker').datepicker({
        language: 'ru',
        orientation: "bottom auto",
        todayHighlight: true
    });
    $('.date-control .dateinput').on('focus', function() {
        $('.date_picker').show();
    });
    $('.date-control .date_picker').on('changeDate', function() {
        $(this).hide();
        $('.date-control input').val(
            $(this).datepicker('getFormattedDate')
        );
    });

    //Reset rating
    $("[type='reset']").click(function() {
        $(".form-rates input").removeAttr('checked');
    });

    //Toggle search dropdown
    $(document).on('click', '.main_heading_toggle:not(.active)', function(){
        $('.main_heading_toggle').toggleClass('active');
        $('.form-wrapper .search_dropdown').toggleClass('hidden');
    });

    //Gender radios
    $(".filter-radio").change(function() {
        var checked = $(this).is(':checked');
        $(".filter-radio").prop('checked',false);
        if(checked) {
            $(this).prop('checked',true);
        }
    });

});

var map1 = new YMaps.Map(YMaps.jQuery("#map1")[0]);
var map2 = new YMaps.Map(YMaps.jQuery("#map2")[0]);
var map3 = new YMaps.Map(YMaps.jQuery("#map3")[0]);

map1.setCenter(new YMaps.GeoPoint(37.609,55.753), 10);
map2.setCenter(new YMaps.GeoPoint(30.313,59.938), 10);
map3.setCenter(new YMaps.GeoPoint(30.313,59.938), 10);

